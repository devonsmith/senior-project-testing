# Western Oregon University Senior Project 2018 #

This README will contain any information required to setup the project in your development environment 
when the repository contains project code.

### What is this repository for? ###

This is the repository containing all of the senior project components for Abby Horner, Devon Smith, 
Jacob Hatfield, and Stephen Oliver. This repository will contain all project files and application code.

### Contribution guidelines ###

You can find our contribution guidelines [here](contribution.md)

### Vision Statement ###
For students and professionals who need to quickly and effectively communicate with their virtual information, 
Remi is a web application that provides a conversational interface that will allow these users to communicate 
with a computer system using their voices. This application will display requested information to the user 
based on the intent of their queries. For example, if a user requests a look at their schedule, Remi will 
display any upcoming events as listed on the user’s calendar. This software will help users quickly and 
effectively interact with a computer system and provide them with a simple interface that aggregates their 
online services and data. Unlike other conversational AIs, Remi is able to recognize the user and access their 
specific data via voice recognition.

### Team Members ###
* [Abby Horner](https://ridethatcyclone.github.io/)
* [Devon Smith](devonsmith.github.io)
* [Jake Hatfield](https://jthatfield15.github.io/cs460/)
* [Stephen Oliver](https://skoliver89.github.io/)

### Team Song ###
[Derezzed by Daft Punk](https://open.spotify.com/track/5X4ojuZG2mZ68EcLyBQ1D3?si=autxSQ0pQvmcr3fCq07ltQ)