﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Remi.Controllers
{
    public class DictionaryController : Controller
    {
        public JsonResult GetDefinition()
        {
            System.Diagnostics.Debug.WriteLine("Made it to the controller");
            // Get the keys
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["OxfordDictionaryKey"];
            string id = System.Web.Configuration.WebConfigurationManager.AppSettings["OxfordDictionaryID"];

            string word_id = Request.QueryString["word"].ToLower(); // word_id must be lowercase

            // Build the url
            string Url = "https://od-api.oxforddictionaries.com/api/v1/entries/en/" + word_id;

            // Create the request
            WebRequest request = WebRequest.Create(Url);

            // Set headers
            request.Headers.Add("app_id", id);
            request.Headers.Add("app_key", key);

            // Get response
            WebResponse response = request.GetResponse();

            // Start data stream
            Stream stream = response.GetResponseStream();

            string reader = new StreamReader(stream).ReadToEnd();

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var defResponse = serializer.DeserializeObject(reader);

            stream.Close();
            response.Close();

            return Json(defResponse, JsonRequestBehavior.AllowGet);


        }
    }
}