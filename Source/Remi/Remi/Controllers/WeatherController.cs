﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Remi.Controllers
{
    public class WeatherController : Controller
    {
        //API Call to/from Weather Underground
        //LOGO MUST BE DISPLAYED PER USER AGREEMENT: https://www.wunderground.com/weather/api/d/docs?d=resources/logo-usage-guide
        public JsonResult ShowWeather()
        {
            // --- Create an API Request String ---
            //Retrive the API Key from a secret location (can only be used for public google stuff)
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["WeatherKey"];

            //Get the user's text input 
            string textInput = Request.QueryString["q"];

            var splitString = textInput.Split('|');

            var city = splitString[0];
            var state = splitString[1];

            //Generate an API Request URL string
            string url = "http://api.wunderground.com/api/" + key + "/conditions/q/" + state + "/" + city + ".json";

            // --- Send the request and get a response from Google API Server ---
            //Send a request to Google
            WebRequest request = WebRequest.Create(url);
            //Get the response from Google
            WebResponse response = request.GetResponse();
            //Start the data stream
            Stream stream = response.GetResponseStream();
            //read the stream in as a string
            string reader = new StreamReader(stream).ReadToEnd();

            // --- Parse the reader string into a JSON Object that we can use ---
            //initialize the serializer
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //use the serializer to parse the 'reader' string into a JSON Object
            var weatherResponse = serializer.DeserializeObject(reader);

            // --- Clean up the API Request Connections ---
            stream.Close();
            response.Close();

            return Json(weatherResponse, JsonRequestBehavior.AllowGet);
        }
    }
}