﻿using Remi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml;
using static Remi.Models.GoogleEventsResponse;

namespace Remi.Controllers
{
    //Controller to handle Google Calendar Actions/Requests
    public class GCalendarController : Controller
    {
        /// <summary>
        /// Get Events JSON Object from Google API Server
        /// </summary>
        /// <param name="hours">(optional) 12 or 24 for time format</param>
        /// <param name="type">schedule, agenda, or calendar.</param>
        /// <param name="position">position relative to the current day. 0 = current day, 1 next day, -1 previous day.</param>
        /// <param name="span">String that represents the time span; day, week, month, year.</param>
        /// <returns></returns>
        public JsonResult GetPublicEvents(int? hours, string type, int? position, string span)
        {
            // --- Create an API Request String ---
            //Retrieve the API Key from a secret location (can only be used for public Google stuff)
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["GCalAPIKey"];
            //Get the desired users Public Calendar ID 
            string calendarID = "reviaitest%40gmail.com"; //Temp. hard coded to Remi's test calendar
            //Generate an API Request URL string
            string url = "https://www.googleapis.com/calendar/v3/calendars/" + calendarID +
                "/events?key=" + key;

            // Current date:
            DateTime today = DateTime.Today;

            // Calculate the first day of the week
            DateTime previousSunday = DateTime.Today
                .AddDays(-1 * ((7 + (today.DayOfWeek - DayOfWeek.Sunday)) % 7));
            // Calculate the next Sunday
            DateTime nextSunday = previousSunday.AddDays(7);
            // Calculate the first day of the month
            DateTime startOfMonth = new DateTime(today.Year, DateTime.Today.Month, 1);
            // Calculate the last day of the month.
            DateTime endOfMonth = startOfMonth.AddMonths(1).AddTicks(-1);
            // Calculate the first day of the year
            DateTime startOfYear = new DateTime(today.Year, 1, 1);
            // Calculate the last day of the year
            DateTime endOfYear = new DateTime(today.Year, 12, 31);

            if (position.HasValue && span != null)
            {
                switch (position)
                {
                    // If we're going back in time
                    case -1:
                        switch (span)
                        {
                            case ("week"):
                                // add the Sunday before the previous Sunday.
                                url += "&timeMin="
                                    + XmlConvert.ToString(previousSunday.AddDays(-7),
                                      XmlDateTimeSerializationMode.Local);
                                // End at the previous Sunday.
                                url += "&timeMax="
                                    + XmlConvert.ToString(previousSunday.AddDays(-1),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            case ("month"):
                                // Start at the beginning of the previous month.
                                url += "&timeMin="
                                    + XmlConvert.ToString(startOfMonth.AddMonths(-1),
                                      XmlDateTimeSerializationMode.Local);
                                // End at the start of the current month - 1 day.
                                url += "&timeMax="
                                    + XmlConvert.ToString(endOfMonth.AddMonths(-1),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            case ("year"):
                                // End at the start of the previous year
                                url += "&timeMin="
                                    + XmlConvert.ToString(startOfYear.AddYears(-1),
                                      XmlDateTimeSerializationMode.Local);
                                // end at the start of this year - 1 day.
                                url += "&timeMax="
                                    + XmlConvert.ToString(startOfYear.AddDays(-1),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            default:
                                // The default time span is one day.
                                url += "&timeMin="
                                    + XmlConvert.ToString(DateTime.Today.AddDays(-1),
                                      XmlDateTimeSerializationMode.Local);
                                url += "&timeMax="
                                    + XmlConvert.ToString(DateTime.Today,
                                      XmlDateTimeSerializationMode.Local);
                                break;
                        }
                        break;
                    // if we're going forward in time
                    case 1:
                        switch (span)
                        {
                            case ("week"):
                                // Start at the next Sunday.
                                url += "&timeMin="
                                    + XmlConvert.ToString(nextSunday,
                                      XmlDateTimeSerializationMode.Local);
                                // End at the end of this week.
                                url += "&timeMax="
                                    + XmlConvert.ToString(nextSunday.AddDays(6),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            case ("month"):
                                // Start at the beginning of the next month
                                url += "&timeMin="
                                    + XmlConvert.ToString(startOfMonth.AddMonths(1),
                                      XmlDateTimeSerializationMode.Local);
                                // Stop at the end of the next month.
                                url += "&timeMax="
                                    + XmlConvert.ToString(endOfMonth.AddMonths(1),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            case ("year"):
                                // do stuff
                                url += "&timeMin="
                                    + XmlConvert.ToString(startOfYear.AddYears(1),
                                      XmlDateTimeSerializationMode.Local);
                                url += "&timeMax="
                                    + XmlConvert.ToString(endOfYear.AddYears(1),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            default:
                                // do stuff
                                url += "&timeMin="
                                    + XmlConvert.ToString(DateTime.Today.AddDays(1),
                                      XmlDateTimeSerializationMode.Local);
                                url += "&timeMax="
                                    + XmlConvert.ToString(DateTime.Today.AddDays(2),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                        }
                        break;
                    // Otherwise, we're going to work with the current time period.
                    default:
                        switch (span)
                        {
                            case ("week"):
                                url += "&timeMin="
                                    + XmlConvert.ToString(previousSunday,
                                      XmlDateTimeSerializationMode.Local);
                                url += "&timeMax="
                                    + XmlConvert.ToString(nextSunday.AddDays(-1),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            case ("month"):
                                url += "&timeMin="
                                    + XmlConvert.ToString(startOfMonth,
                                      XmlDateTimeSerializationMode.Local);
                                url += "&timeMax="
                                    + XmlConvert.ToString(endOfMonth,
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            case ("year"):
                                url += "&timeMin="
                                    + XmlConvert.ToString(startOfYear,
                                      XmlDateTimeSerializationMode.Local);
                                url += "&timeMax="
                                    + XmlConvert.ToString(endOfYear,
                                      XmlDateTimeSerializationMode.Local);
                                break;
                            default:
                                url += "&timeMin="
                                    + XmlConvert.ToString(DateTime.Today.Date,
                                      XmlDateTimeSerializationMode.Local);

                                url += "&timeMax="
                                    + XmlConvert.ToString(DateTime.Today.Date.AddHours(24),
                                      XmlDateTimeSerializationMode.Local);
                                break;
                        }
                        break;
                }
            }

            // This is a fix for the Google Calendar API not accepting positive offsets
            // with the '+' character. Here we'll replace the '+' with '%2B' 
            url = Regex.Replace(url, @"\+", "%2b");
            // Debugging line for Google URL.
            Debug.WriteLine("Google URL String: " + url);
            try
            {
                // --- Sent the request and get a response from Google API Server ---

                //Send a request to Google
                WebRequest request = WebRequest.Create(url);
                //Get the response from Google
                WebResponse response = request.GetResponse();
                //Start the data stream
                Stream stream = response.GetResponseStream();

                // Testing the output of the stream
                Console.Write(stream);

                // --- Parse the reader string into a JSON Object that we can use ---
                //initialize the serializer
                //use the serializer to parse the 'reader' string into a JSON Object
                var events = new System.Web.Script.Serialization.JavaScriptSerializer()
                                        .Deserialize<GoogleEventsRootObject>(new StreamReader(stream)
                                        .ReadToEnd()); ;

                // --- Clean up the API Request Connections ---
                stream.Close();
                response.Close();

                // Make a list of events to return.
                List<CalendarEvent> eventList = new List<CalendarEvent>();

                foreach (GoogleEventItem item in events.items)
                {
                    // Create a new calendar event item.
                    CalendarEvent newEvent = new CalendarEvent();
                    newEvent.EventName = item.summary;
                    newEvent.EventDate = item.start.dateTime.ToShortDateString();
                    // if the user requests the value in 12-hour time.
                    if (Nullable.Compare<int>(hours, 12) == 0)
                    {
                        // Create a string builder to setup the 12 hour time.
                        StringBuilder sb = new StringBuilder();
                        // Creating the start date.
                        // Append the hour.
                        sb.Append(Convert.ToString((item.start.dateTime.Hour > 12) ? (item.start.dateTime.Hour - 12) : item.start.dateTime.Hour));
                        sb.Append(":");
                        // Append the minute.
                        sb.Append(Convert.ToString(item.start.dateTime.Minute));
                        sb.Append(Convert.ToString(item.start.dateTime.Second));
                        sb.Append((item.start.dateTime.Hour < 12) ? "AM" : "PM");
                        newEvent.EventStartTime = sb.ToString();
                        // clear the string builder so we can use it again.
                        sb.Clear();
                        // Creating the end date.
                        sb.Append(Convert.ToString((item.end.dateTime.Hour > 12) ? (item.end.dateTime.Hour - 12) : item.end.dateTime.Hour));
                        sb.Append(":");
                        // Append the minute.
                        sb.Append(Convert.ToString(item.end.dateTime.Minute));
                        sb.Append(Convert.ToString(item.end.dateTime.Second));
                        // Append AM or PM
                        sb.Append((item.end.dateTime.Hour < 12) ? "AM" : "PM");
                        newEvent.EventEndTime = sb.ToString();
                    }
                    else
                    {
                        newEvent.EventStartTime = item.start.dateTime.ToShortTimeString();
                        newEvent.EventEndTime = item.end.dateTime.ToShortTimeString();
                    }
                    // Add the item to the event list.
                    eventList.Add(newEvent);
                }

                // Return the Json Result
                return Json(eventList, JsonRequestBehavior.AllowGet);
            }
            // If there is an error, we're going to build a dummy calendar
            // event to share the data with the client.
            catch (WebException we)
            {
                // Write the error to the debug console.
                Debug.Write(we.Message);
                // Make a dummy list of events
                List<CalendarEvent> eventList = new List<CalendarEvent>();
                // Make a dummy calendar item to store the error message.
                CalendarEvent item = new CalendarEvent();
                item.EventName = "Web Request Error: " + we.Message;
                item.EventStartTime = DateTime.Now.ToShortTimeString();
                item.EventEndTime = DateTime.Now.ToShortTimeString();
                item.EventDate = DateTime.Now.ToShortDateString();
                eventList.Add(item);
                // Send the list containing the error dummy item.
                return Json(eventList, JsonRequestBehavior.AllowGet);
            }
            // If there is any exception other than a web exception
            // make a dummy event with that data.
            catch (Exception e)
            {
                // Write the error to the debug console.
                Debug.Write(e.Message);
                // Make a dummy list of events
                List<CalendarEvent> eventList = new List<CalendarEvent>();
                // Make a dummy calendar item to store the error message.
                CalendarEvent item = new CalendarEvent();
                item.EventName = e.Message;
                item.EventStartTime = DateTime.Now.ToShortTimeString();
                item.EventEndTime = DateTime.Now.ToShortTimeString();
                item.EventDate = DateTime.Now.ToShortDateString();
                eventList.Add(item);
                // Send the list containing the error dummy item.
                return Json(eventList, JsonRequestBehavior.AllowGet);
            }
        }
    }
}


