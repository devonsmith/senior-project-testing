﻿var audioContext = null;
var canvas = null;
var context = null;
var frameRequest = null;

// When the window loads start to setup the audio context.
window.onload = micCheck();

// The text input for the renderer.
var remiSpeech = '';

// Moved to a defined function so it could be re-executed when a button is clicked...
function micCheck () {
    // get the audio context using the standard method or the Webkit method
    // note: there is also a Mozilla-specific version but standard is preferred.
    window.AudioContext = window.AudioContext || window.webkitAudioContext;

    // setup the new audio context object.
    audioContext = new AudioContext();

    // Attempt to get the audio input
    try {
        // Using getUserMedia from various standard and non-standard sources.
        // Aliases the various versions of getUserMedia.
        navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

        // Attempt to get the audio input.
        navigator.getUserMedia(
            {
                "audio": {
                    "mandatory": {
                        "googEchoCancellation": "false",
                        "googAutoGainControl": "false",
                        "googNoiseSuppression": "false",
                        "googHighpassFilter": "false"
                    },
                    "optional": []
                },
            }, success, failure);
    }
    // if something bad happened.
    catch (e) {
        alert('getUserMedia threw exception :' + e);
    }
    // if the stream was successful.
    function success(stream) {
        // Let the user know Remi can hear them.
        outputRemiChat("Hey, I can hear you! That's great!");
        $('#levels').empty();
        $('#levels').append("Yep, your microphone is working!");
        // audio node.
        mediaStreamSource = audioContext.createMediaStreamSource(stream);
        // create the meter.
        meter = createAudioMeter(audioContext);
        mediaStreamSource.connect(meter);
        // start the animation of remi!
        draw();
    }
    // if the stream failed to initialize.
    function failure() {
        $('#levels').empty();
        $('#levels').append("Your microphone isn't working. Have you tried the steps above?");
        alert('I can\'t hear you! Q.Q');
    }

    // Here is where the meter gets created.
    function createAudioMeter(audioContext, clipLevel, averaging, clipLag) {
        var processor = audioContext.createScriptProcessor(512);
        processor.onaudioprocess = volumeAudioProcess;
        processor.clipping = false;
        processor.lastClip = 0;
        processor.volume = 0;
        processor.clipLevel = clipLevel || 0.98;
        processor.averaging = averaging || 0.95;
        processor.clipLag = clipLag || 750;

        // From Audio Meter demonstration:
        // this will have no effect, since we don't copy the input to the output,
        // but works around a current Chrome bug.
        processor.connect(audioContext.destination);

        processor.checkClipping =
            function () {
                if (!this.clipping)
                    return false;
                if ((this.lastClip + this.clipLag) < window.performance.now())
                    this.clipping = false;
                return this.clipping;
            };

        processor.shutdown =
            function () {
                this.disconnect();
                this.onaudioprocess = null;
            };

        return processor;
    }

    function volumeAudioProcess(event) {
        var buf = event.inputBuffer.getChannelData(0);
        var bufLength = buf.length;
        var sum = 0;
        var x;

        // Do a root-mean-square on the samples: sum up the squares...
        for (var i = 0; i < bufLength; i++) {
            x = buf[i];
            if (Math.abs(x) >= this.clipLevel) {
                this.clipping = true;
                this.lastClip = window.performance.now();
            }
            sum += x * x;
        }
        // get the RMS
        var rms = Math.sqrt(sum / bufLength);
        // Smooths out the averaging process.
        this.volume = Math.max(rms, this.volume * this.averaging);
    }

    // variables placed here for easier editing.
    // This is the starting size for Remi's character. This can be updated to something
    // that is smaller or larger depending on the context of the animation and where it will
    // be placed on the page.
    var base_size = 150;


    
    // Draw function, will draw to the canvas on the page.
    function draw() {
        var outputText = remiSpeech;
        // I couldn't find a way to do this with JQuery, so here it is in traditional js.
        var canvas = document.getElementById('canvas');
        // set the resolution of the canvas
        canvas.width = 1280;//horizontal resolution (?) - increase for better looking text
        canvas.height = 720;//vertical resolution (?) - increase for better looking text
        // get the center point of the canvas so we can place Remi in the middle.
        // center for horizontal, middle for vertical.
        var center = canvas.width / 2;
        var middle = canvas.height / 2;

        // set the radius to the size that we want depending on the microphone volume.
        var radius = base_size - meter.volume * 700;
        //limit the shrunk size so we don't break the draw. If the radius reaches 0
        // the drawing of the objects will stop.
        var minLimit = base_size * 0.3;
        if (radius <= minLimit) {
            radius = minLimit;
        }
        // Make sure we can get the context for the canvas if we can't, ignore.
        if (canvas.getContext) {
            // Get the 2d context for the canvas.
            var context = canvas.getContext('2d');
            // Add aliasing to the graphics context
            //context.imageSmoothingEnabled = true;
            // Clear the canvas of any previous information before created the frame.
            context.clearRect(0, 0, canvas.width, canvas.height);

            /****************** Inner Circle ***************************/
            // Here is where we draw the basic circle before highlight.
            context.beginPath();
            // Draw and arch in the radius of 2*Pi to make the circle.
            context.arc(center, middle, radius, 0, Math.PI * 2, true);
            // Fill the path with purple.
            context.fillStyle = 'purple';
            context.fill();
            // set the line width.
            context.lineWidth = 4;
            // set the color of the outer stoke.
            context.strokeStyle = '#330033';
            context.stroke();

            // TODO: Implement this gradient on the reflection.
            // var gradient = context.createLinearGradient(0,500,0, 0);
            // gradient.addColorStop(0, 'white');
            // gradient.addColorStop(0.5, 'plum');
            // gradient.addColorStop(1, 'purple');

            /******************** Eye Highlight ***********************/
            // Draw the highlight on the eye.
            context.beginPath();
            context.arc(center, middle, radius - (radius * 0.25), (0.5 * Math.PI), 0, true);
            context.lineWidth = radius * 0.125;
            // stroke with gradient
            context.strokeStyle = 'rgba(255, 255, 255, 0.5)';
            // old solid stroke
            // context.strokeStyle = 'white'
            context.stroke();

            /******************** Eye Glint ****************************/

            /********************* Text Output *************************/
            if($('#checktextout').is(":checked")){
                //var outputText = remiSpeech;
                // Text format variables.
                context.font = '24pt Arial';
                // the height of the lines
                var lineHeight = 40;
                context.fillStyle = '#eee';
                // This sets the text element to be positioned by its center point.
                context.textAlign = 'center';
                var wordArray = outputText.split(' ');
                var line = '';
                var x = canvas.width / 2;
                var y = canvas.height - (lineHeight * 3.5);
                var lineWidth = canvas.width - (canvas.width * 0.20);

                for (var i = 0; i < wordArray.length; i++) {
                    var testLine = line + wordArray[i] + ' ';
                    if (context.measureText(testLine).width > lineWidth && i > 0) {
                        context.fillText(line, x, y);
                        line = wordArray[i] + ' ';
                        y += lineHeight;
                    }
                    else {
                        line = testLine;
                    }
                }
                context.fillText(line, x, y);
            }
        }


        // get the next callback for the frame
        frameRequest = window.requestAnimationFrame(draw);
    }
}
