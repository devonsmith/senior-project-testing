﻿//Global Variables Here
//. . . 

//Listener Here
//clicked the login/user link
$("#userLogin").click(userLoginHelper);
//clicked the logout link
$("#userLogout").click(userLogout);
var user = '';
//Functions Here
//Helper function to change the User Info link in top corner of screen.
function userLoginHelper() {
    var status = $("#userLogin").val();
    console.log(status);
    user = 'Test User';
    if (status == 0) {
        console.log("Not currently logged in.");
        userLogin(user)
    }
    else {
        var currUser = $("#userLogin").text();
        console.log(currUser + " currently logged in.");
    }
}

//After we have authentication, refactor to change this once the user it logged in
//For testing we will just fill the user info nav item with a test user.
function userLogin(user) {
    console.log("Log in a user. . .");
    $("#userLogin").val(true); //indicate in the value that the user is logged in
    $("#userLogin").html('<a class="navbar-brand" href="#">' + user + '</a > ');
    $("#userLogout").css("display", "flex");
}

//After we hat authentication, refactor to fall in-line with needs
//Function to logout the test user that we "logged in"
function userLogout() {
    console.log("Log out the user. . .")
    $("#userLogout").css("display", "none"); //stop displaying the logout link
    $("#userLogin").val(false); //indicate in the value that user is logged out
    $("#userLogin").html('<a class="navbar-brand" href="#">Login</a>'); //change the user info back to login
}