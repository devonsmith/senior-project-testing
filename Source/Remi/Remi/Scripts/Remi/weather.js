﻿// Collection of abbreviated state names
var abbreviatedState = {
    'alabama': 'AL',
    'alaska': 'AK',
    'arizona': 'AZ',
    'arkansas': 'AR',
    'california': 'CA',
    'colorado': 'CO',
    'connecticut': 'CT',
    'delaware': 'DE',
    'florida': 'FL',
    'georgia': 'GA',
    'hawaii': 'HI',
    'idaho': 'ID',
    'illinois': 'IL',
    'indiana': 'IN',
    'iowa': 'IA',
    'kansas': 'KS',
    'kentucky': 'KY',
    'louisiana': 'LA',
    'maine': 'ME',
    'maryland': 'MD',
    'massachusetts': 'MA',
    'michigan': 'MI',
    'minnesota': 'MN',
    'mississippi': 'MS',
    'missouri': 'MO',
    'montana': 'MT',
    'nebraska': 'NE',
    'nevada': 'NV',
    'new hampshire': 'NH',
    'new jersey': 'NJ',
    'new mexico': 'NM',
    'new york': 'NY',
    'north carolina': 'NC',
    'north dakota': 'ND',
    'ohio': 'OH',
    'oklahoma': 'OK',
    'oregon': 'OR',
    'pennsylvania': 'PA',
    'rhode island': 'RI',
    'south carolina': 'SC',
    'south dakota': 'SD',
    'tennessee': 'TN',
    'texas': 'TX',
    'utah': 'UT',
    'vermont': 'VT',
    'virginia': 'VA',
    'washington': 'WA',
    'west virginia': 'WV',
    'wisconsin': 'WI',
    'wyoming': 'WY'
};


//Function to get the weather from Weather Underground
function getWeather(entities) {
    var queryCity = "Monmouth"; // by default
    var queryState = "OR"; // by default

    // Place the Weather Underground branding on the page.
    // Empty the filler pane to prepare for new info
    $('#fillerPane').empty();
    var weatherBranding = ''
        + '<div class="text-right">'
        + '<div class="alert alert-dismissible alert-light">'
        + '        <p>Weather Provided By:</p>'
        + '        <img src="https://icons.wxug.com/logos/PNG/wundergroundLogo_4c_horz.png" width="140" />'
        + '</div>'
        + '</div>';
    // Put the new information into the filler pane.
    $('#fillerPane').append(weatherBranding);

    for (i = 0; i < entities.length; i++) {
        switch (entities[i][0]) {
            case "City":
                queryCity = entities[i][1];
                queryCity = replaceSpacesWithUnderscore(initialCaps(queryCity));
                break;
            case "State":
                queryState = entities[i][1];
                queryState = abbreviatedState[queryState.toLowerCase()];
                break;
        }
    }
    console.log("queryCity: " + queryCity + "");
    var source = "/weather/?q=" + queryCity + '|' + queryState;
    console.log("source: " + source + "");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: sendWeather,
        error: logFailure
    });
}

//Function to send weather conditions to Remi to display to the user
function sendWeather(response) {
    var outputString = "";
    console.log(response);
    console.log("result is null?: " + (response.response.results == null) + "");
    if ((response.response.results == null) == false) {
        outputString = "I couldn't find that location.";
        outputRemiChat(outputString);
    }
    else {
        var temp = response.current_observation.temp_f;
        var location = response.current_observation.display_location.full
        outputString = "It is currently " + temp + "°F in " + location + ".";
        if (temp <= 50) {
            outputString += " Don't forget your jacket!"
        }
        outputRemiChat(outputString);
    }
}

// Replaces spaces with _ (underscore)
function replaceSpacesWithUnderscore(inputText) {
    var replacedText = inputText.split(" ").join("_");
    return replacedText;
}