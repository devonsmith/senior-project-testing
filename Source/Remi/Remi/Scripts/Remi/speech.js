﻿// --- Global Variables here ---
//...

// --- Listeners here ---
//Temp button to demo the response for when Remi understands the user's intent
//$("#remisValidLanguageRequest").click(validInput);
//Temp button to demo the response for when Remi does NOT understand the user's intent
//$("#remisInvalidLanguageRequest").click(invalidInput);
//Button to submit text to Remi
$("#submitremitextinput").click(sendRemiTextInput);

//--- Functions here ---
//Testing function - Simulate a valid user Input
// and then execute the processTextLanguage function
/*function validInput() {
    console.log("valid pushed");
    var textInput = "Hello%20Remi."; //all spaces must be represented with '%20'
    processTextLanguage(textInput);
}*/
//Testing function - Simulate an invalid user Input
// and then execute the processTextLanguage function
/*function invalidInput() {
    console.log("invalid pushed");
    var textInput = "What%20is%20the%20meaning%20of%20life?";
    processTextLanguage(textInput);
}*/

//Function to let Remi know if the user is logged in or not
//Some refactoring may be required once real auth is put in
function isLoggedIn() {
    var userStatus = $("#userLogin").val();
    //console.log("userStatus: " + userStatus)
    if (userStatus == 0) {
        return false;
    }
    else {
        return true;
    }
}

//Funtion to send Remi's input to LUIS via processTextLanguage()
function sendRemiTextInput() {
    if (isLoggedIn()) {
        var inputText = document.getElementById("remitextinput").value;
        //console.log(inputText);
        //sending input the JSON, when you have spaces, replace with %20
        inputText = replaceSpaces(inputText);
        console.log(inputText);
        processTextLanguage(inputText);
    }
    else {
        outputString = "Oh no, looks like you are not logged in! Please log in and try again.";
        outputRemiChat(outputString);
    }
}

// Replaces spaces with %20
function replaceSpaces(inputText) {
    var replacedText = inputText.split(" ").join("%20");
    return replacedText;
}

//Function to send AJAX request off to our server
//The users text input is added as a query string to the source
function processTextLanguage(input) {
    var source = "/language/text/?q=" + input;
    console.log(source);
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: getIntent,
        error: logFailure
    });
}

//Function to indicate that the AJAX call broke. (For debugging)
function logFailure(response) {
    console.log(response);
    var outputString = 'Oh no, looks like I am having issues with my language AJAX logic.';
    outputRemiChat(outputString);
}

//Function to determine User intent
function getIntent(response) {
    // Empty the filler pane for reuse.
    $('#fillerPane').empty();
    console.log(response);
    var intent = response.topScoringIntent.intent;
    var entities = [];

    for (i = 0; i < response.entities.length; i++) {
        var tempEntity = []; 

        tempEntity[0] = response.entities[i].type;
        tempEntity[1] = response.entities[i].entity;
        entities.push(tempEntity);
    }
      
    switch (intent) {
        case ('RemiGreeting'):
            outputString = 'Hello ' + user + ', how can I help you?';
            outputRemiChat(outputString);
            break;

        case ('RemiJoke'):
            getJoke();
            break;

        case ("GetCalendarData"):
            getPublicEvents(response.entities);
            break;

        case ("GetWeather"):
            getWeather(entities);
            break;

        case ("GetDefinition"):
            getDefinition(entities);
            break;

        default:
            outputString = "I am sorry, I do not understand your request. Please rephrase it and try again.";
            outputRemiChat(outputString);
            break;
    }
}
// Helper function to update the message output to the canvas.
function outputRemiChat(remisResponse) {
    remiSpeech = remisResponse;
    console.log("Response: " + remiSpeech);
}

// --- Functions to enable Remi's sense of Humor ---
// --- These functions may move to another JS file in a refactor once we have more Functionality for Remi ---
//function to get a joke from an API
function getJoke() {
    var source = "/language/joke";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: sendJoke,
        error: logFailure
    });
}

//Function to send the joke to Remi to display to the user
function sendJoke(response) {
    var outputString = response.joke;
    outputRemiChat(outputString);
}

// Takes in a string, sets it all to lower case and then capitalized the first letter of every word by splitting at the space
// param: String
function initialCaps(incommingString) {
    var returnString = "";
    var tempArray = null;

    incommingString = incommingString.toLowerCase();

    tempArray = incommingString.split(" ");

    for (var i = 0; i < tempArray.length; i++) {
        returnString += tempArray[i].charAt(0).toUpperCase() + tempArray[i].substring(1, tempArray[i].length) + " ";
    }

    returnString = returnString.trim();

    return returnString;
}
