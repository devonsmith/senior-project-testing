﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CS461_M3_Cross_Country.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// The landing page for all users.
        /// </summary>
        /// <returns>The site index.</returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}