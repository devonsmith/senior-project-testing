﻿using CS461_M3_Cross_Country.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CS461_M3_Cross_Country.Controllers
{
    public class ParentsController : Controller
    {
        private XCDatabaseContext db = new XCDatabaseContext();
        // GET: Parents
        public ActionResult Index()
        {
            // send parent data for temp psuedo-login links for work prior to authentication is added.
            var parents = db.Parents.ToList();
            return View(parents);
        }

        // GET: Parents/ParentHome/{ParentID}
        // Using the parent id generate a homepage for parents to view the stats of their child(ren) (athlete(s))
        // This method will become the method for the parent index view when authentication is added and proven functional.
        public ActionResult ParentHome(int id)
        {
            // Just a quick way to send the parent name over to the view and still send athlete data with the view (obviously this would get refactored to a ViewModel later on).
            // However, the viewbag is being used to prove the arcitecture simply.
            ViewBag.ParentName = db.Parents.Where(p => p.ParentID == id).Select(ps => ps.FirstName).FirstOrDefault().ToString() + " " + 
                db.Parents.Where(pw => pw.ParentID == id).Select(ps => ps.LastName).FirstOrDefault().ToString();

            // Get The Parent's Athletes' data
            var athleteIDs = db.ParentsAthletes.Where(w => w.ParentID == id).Select(s => s.AthleteID).ToList();
            var athletes = new List<Athlete>();
            foreach(int aID in athleteIDs)
            {
                var athlete = db.Athletes.Where(a => a.AthleteID == aID).FirstOrDefault();
                athletes.Add(athlete);
            }
            
            return View(athletes);
        }

        // GET: Parents/AthleteSessions/{AthleteID}
        // This method processes data about an individual athlete sessions (related to the parent).
        // The View will be spilt into tables for Performance Metrics and Attendance Dates/Times to make it easier for a parent to make sense of the data.
        // Since there is no real authentication yet there is no way to prevent parents from seeing other athlete data (not related to them)
        // if they know how to leverage the routes.
        public ActionResult AthleteSessions(int id)
        {
            ViewBag.AthleteName = db.Athletes.Where(w => w.AthleteID == id).Select(s => s.FirstName).FirstOrDefault() + " " + db.Athletes.Where(w => w.AthleteID == id).Select(s => s.LastName).FirstOrDefault();
            var sessions = db.Sessions.Where(a => a.AthleteID == id).OrderByDescending(x => x.TrainingDate).ToList();
            return View(sessions);
        }
    }
}