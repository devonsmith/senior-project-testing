﻿using CS461_M3_Cross_Country.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CS461_M3_Cross_Country.Controllers
{
    public class SessionController : Controller
    {
        private XCDatabaseContext db = new XCDatabaseContext();

        // GET: Coaches
        public ActionResult Index()
        {
            var sessions = db.Sessions.ToList();
            return View(sessions);
        }


        // GET: Athletes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = db.Sessions.Find(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            return View(session);
        }
        
        // GET: Athletes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Session session = db.Sessions.Find(id);
            if (session == null)
            {
                return HttpNotFound();
            }
            return View(session);
        }

        // POST: Athletes/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (ModelState.IsValid)
            {
                Session session = db.Sessions.Find(id);
                db.Sessions.Remove(session);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Delete", id);
        }
    }
}